from .conscious.aware import Aware
from .subconscious.think import Think
from .social.behaviour import Behaviour