from lib.helper import *
class Memory:
    
    # max size of arrays - sizes can be dynamically increased or decreased depending on current situation
    presence_size = 20 # needs to stay small # may SCALE WITH INPUT RATE
    memory_size = 20 # may SCALE WITH INPUT RATE
    # longterm_average_size = 100
    
    
    
    presence = list() # has impressions # more like shortterm memory
    memorys = list() # has memorized # data gets compacted into different event chain later (remove none interacted elements etc.)
    past = list() # has commemoration

    
    
    # splitting this up is important for later thought
    # in order to access present states fast, there needs to be a small easy to access object
    # moving from present to memory compacts event chains with placeholder objects if possible
    # moving from hold to fade deletes elements from list if possible - deletion is also based on interactions
    
    def keep(self, impression): # SHORT - these functions are individual
        where(__name__)
        # self.store = impression # wraps the impression
        self.presence.append(impression)
        
        while len(self.presence) > self.presence_size:
            self.hold(self.oldest(self.presence))
    
    def hold(self, memorized): # MIDDLE - these functions are individual        
        where(__name__)
        self.memorys.append(memorized)

        while len(self.memorys) > self.memory_size:
            self.fade(self.oldest(self.memorys))
    
    def fade(self, commemoration): # LONG # move chain to past - data might get lost... - these functions are individual
        where(__name__)
        average = self.average(self.memorys)

        if commemoration.interactions >= average:
            self.past.append(commemoration)
        
        
        
    def average(self, memorys): # meridian is better for huge memory piles, but not as accurat
        where(__name__)
        count = float()
        for memory in memorys:
            count += memory.interactions
            
        if count == 0: result = float() # handle zero division
        else: result = float(len(memorys))/count
        
        return result
            
    def oldest(self, list):
        where(__name__)
        return list.pop(0)

    def interact(self, aware, count=1): # can only be used by thinking or aware related
        where(__name__)
        aware.interactions += count

