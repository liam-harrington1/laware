from lib.helper import *

class Sense:
    situation = dict() # this can tell you the last value of a 'sense'

    aware = None
        
    def __init__(self, aware):
        self.aware = aware

    def grasp(self, aware):
        where(__name__)
        
        self.datatype = self.type(aware.provide())
        
        self.appropriate(aware)
            
    def appropriate(self, aware): # introduce a sense / handle change
        where(__name__)
        # if not self.datatype in self.situation.keys():
        self.situation[self.datatype] = aware.provide()
            
    def type(self, key): # may be determined in a nother way
        where(__name__)
        return type(key)
            
    def state(self, key):
        where(__name__)
        return self.situation.get(self.type(key))

    def exists(self, key):
        where(__name__)
        return key in self.situation
