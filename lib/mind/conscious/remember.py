from lib.helper import *
class Remember: # REMEMBER TO ADD INTERACTION - No interaction on searching for memory
    # try to reduce overall usage of remembering to boost performance
    aware = None

    def __init__(self, aware):
        where(__name__)
        self.aware = aware

    def lately(self, information):
        where(__name__)
        for stored in self.aware.memorization.presence:
            if information == stored.information:
                self.aware.memorization.interact(stored)
                return True
        return False

    def known(self, information):
        where(__name__)
        for stored in self.aware.memorization.memorys:
            if information == stored.information:
                self.aware.memorization.interact(stored)
                return True
        return False

    def past(self, information):
        where(__name__)
        for stored in self.aware.memorization.past:
            if information == stored.information:
                self.aware.memorization.interact(stored)
                return True
        return False



    def recall(self, information):
        where(__name__)
        remembering = self.aware.remembering

        if remembering.lately(information) or remembering.known(information) or remembering.past(information): return information

    def blanked(self, information): # returns, if not stored - should feed into aware.of
        where(__name__)
        return not bool(self.recall(information))