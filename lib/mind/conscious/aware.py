from lib.helper import *
from . import Sense, Memory, Remember

class Aware(): # Aware can interact with all consious things
    # provide all core functionallity through class var and __init__
    existance = None # set by __init__
    think = None # set by __init__
    behaviour = None # set by __init__
    
    remembering = None

    sensing = None
    memorization = Memory()
    

    def __init__(self, existance, think, behaviour): # used by Exist class
        where(__name__)
        self.existance = existance
        self.think = think
        self.behaviour = behaviour

        self.remembering = Remember(self) # pass aware to remembering

        self.sensing = Sense(self)



    def of(self, information):
        where(__name__)
        self.information = information # use information for non interaction
        self.before = self.sensing.state(information)
        print(self.before, ' -> ', information)
        self.interactions = int()
        
        

        self.assess() # has to be used befor grasp - works without - needs to be improved...

        self.experience()
                   
        self.sensing.grasp(self) # changes sensing state - must be used after notice()

    def provide(self): # Aware cannot provide to itself
        where(__name__)
        self.memorization.interact(self)
        return self.information
    
    def assess(self): # if data of sensing changes, it gets noticed - probably not haw this works, thats why its commented...
        where(__name__)
        if self.sensing.state(self.information) != self.information:
            self.noticed()

    def noticed(self):
        where(__name__)
        pass

    def experience(self):
        where(__name__)
        self.memorization.keep(self)
        self.think.about(self)