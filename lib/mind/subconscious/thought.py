from lib.helper import *
from .explore import Explore

class Thought:
    explore = Explore()

    def through(self, think):
        where(__name__)
        self.think = think

        possibilities = self.unfold() # used with __dir__
        self.think.aware.of(possibilities)

        self.attempt()

        self.result = None

        return self.result

    def unfold(self):
        thing = self.think.aware.provide()
        abilities = dir(thing)
        possibilities = list()
        where(__name__)
        for abilitie in abilities:
            possibilities.append(getattr(thing, abilitie)) # might feed into a nother thought - whatch out for recursion
        return possibilities

    def attempt(self): # an attempt can be followed by an exploration(has parameters)
        where(__name__)
        if callable(self.think.aware.provide()):
            # for every posibility engage into exploration
            pass
