from lib.helper import *
from . import Thought

class Think: # this is where the fun beginns - long live open source
    thought = Thought()
    
    thinking = None # access last thought

    
    def about(self, aware): # might get a timelimit... / you can think about something, that you don't understand
        where(__name__)
        self.success = bool()
        self.aware = aware

        # if not unterstood:
        self.clarify()

        self.thinking = self.thought.through(self) # might block everything noticed

        self.finish()

    # method-wrapper causes trouble
    def clarify(self): # if you need help undestanding, you go through is step by step
        where(__name__)
        information = self.aware.information

        if information: # sortout NoneType
            if iter(information):
                for thing in information:
                    if self.aware.remembering.blanked(thing): # if im blanked about something, i am aware of it...
                        # needs parent attribute to be set
                        self.aware.of(thing)

    def finish(self): # writing this way reads better
        where(__name__)
        self.success = True # result of thinking

