def where(name, frame=1): # asin to name something
    from sys import _getframe as get
    path = name.split('.')
    what = path[-1]
    how = get(frame).f_code.co_name
    # print(name, g(frame).f_code.co_name)
    print(f'what: {what: <20} how: {how: <20} context: {name}')