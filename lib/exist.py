from lib.helper import *
from .mind import Aware
from .mind import Think
from .mind import Behaviour

def inputSource():
    external = input('Input: ') # for sample data
    if external == 'stop': exit(0) # make it harder to take over world...
    return external

class Exist:
    
    ons = list() # supposed to mean exist on ...
    
    def __init__(self, introduced):
        where(__name__)
        self.aware = Aware(self, Think(), Behaviour())
        
        self.introduced = introduced
        
        self.source(inputSource)

        self.persist()
               
    def source(self, method): # asin to source something
        self.ons.append(method)
                
    def on(self): # supposed to mean exist on ...
        where(__name__)
        # - CAN BE ANY INPUT -------
        
        for source in self.ons:
            if callable(source):
                self.aware.of(source())
            else:
                print('provided source ' + str(source) + ' is not callable')
        
        # --------------------------
    
    def persist(self):
        where(__name__)
        while True: self.on() # until the end on sources